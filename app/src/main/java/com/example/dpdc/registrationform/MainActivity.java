package com.example.dpdc.registrationform;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView result;
    EditText firstName;
    EditText lastName;
    EditText phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstName =(EditText) findViewById(R.id.txtFirstName);
        lastName =(EditText) findViewById(R.id.txtLastName);
        phoneNumber =(EditText) findViewById(R.id.txtPhoneNumber);

        result = (TextView) findViewById(R.id.vResult);

        findViewById(R.id.btnConcat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.setText(firstName.getText().toString() + " " +  lastName.getText().toString() + " " + phoneNumber.getText().toString());

            }
        });

    }

}

